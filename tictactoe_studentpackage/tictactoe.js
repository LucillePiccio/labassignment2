"use strict";

document.addEventListener('DOMContentLoaded', (e) => {
   /* console.log('hello World'); */
   const table = document.getElementById('gameboard');
   const win_mess = document.querySelector('aside');

   let playerCounter = 0;
    let curPlayer = 'X';
    if(playerCounter %2 === 0){
        curPlayer = 'X';
        playerCounter++;
    }
    else{
        curPlayer = 'O';
        playerCounter++;
    }
    const win_element = document.getElementById('winningplayer');

   function on_clicks(event){
    let playerOuput = document.getElementById('curplayer');

    //a
    if(event.target.className === 'xsquare' || event.target.className === "osquare"){
        return;
    } 

    //b
    if(win_mess.style.visibility === 'visible'){
        return;
    }

    //c, d
    if(event.target.textContent === ''){
        event.target.textContent = curPlayer;
        if(curPlayer === 'X'){
            event.target.classList.add('xsquare');
        }
        else{
            event.target.classList.add('osquare');
        }
    }

    //e
    if(curPlayer === 'X'){
        curPlayer = 'O';
        playerCounter++;
    }
    else{
        curPlayer = 'X';
        playerCounter++;
    }

    //f
    playerOuput.textContent = curPlayer;

    //g
    const num_cell = document.querySelectorAll('td');
    const positions = [];
    /* going through all 9 td cells, because there are 9 squares(positions)*/
    for(let i = 0; i < num_cell.length; i++){
        /* checking the value of each td cell to see if they are empty*/
        if (num_cell[i].textContent === ""){
            positions[i] = false;
        }
        else{
            /* inserts the value of each td cell into the positions array if they are not empty */
            positions[i] = num_cell[i].textContent;
        }
    }
    //console.log(positions);
    let results = checkBoard(...positions);
    //console.log(results);

    if(results === 'X'){
        showWinningMessage(results);
    }
    else if(results == 'O'){
        showWinningMessage(results);
    }
}
    table.addEventListener('click', on_clicks);

    function showWinningMessage(winner){
        /* when the results variable is equal to O, win_element will be changed to O and the same for X*/
        if(winner === 'O'){
            win_element.textContent = 'O';
        }
        else if(winner === 'X'){
            win_element.textContent = 'X'
        }
        win_mess.style.visibility = 'visible';
    }

    const reset = document.querySelector('button');
    function new_game(){
        const num_cell = document.querySelectorAll('td');
        for(let i = 0; i < num_cell.length; i++){
            /* removes all classes from board game and makes it empty*/
            num_cell[i].classList.remove('xsquare');
            num_cell[i].classList.remove('osquare');
            num_cell[i].textContent = '';
            /* puts back message into hidden and resets player and the playerCounter*/
            win_mess.style.visibility = 'hidden';
            curPlayer = 'X';
            playerCounter = 0;
        }
    }
    
    reset.addEventListener('click', new_game);
})
